import '../styles/globals.css'

/**
 * It takes a component and page props and returns a React component
 * @returns A function that returns a React component.
 */
function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
